---
title: Stratégie anti-pauvreté
subtitle: Et si la France se dotait d'une Agence aux opportunités économiques?
date: 2018-10-15	
author: Niels Planel
abstract:
keywords:
bibliography:
lang: fr
link-citations: true
image: Signac_003.jpeg
alt: Paul Signac
---

L'annonce de la stratégie anti-pauvreté du gouvernement français le 13 septembre est venue en écho à ce qui est peut-être là une nouvelle «&nbsp;vérité qui dérange&nbsp;»&nbsp;: on le sait désormais, un feu terrible, encore maîtrisable aujourd'hui mais amené à devenir un incendie incandescent demain, couve dans les économies avancées, et il s'agit de l'indigence. [En Europe, aux États-Unis](https://esprit.presse.fr/actualites/collectif/reduire-la-pauvrete-dans-les-pays-riches-41659), le populisme plonge ses racines dans le paupérisme&nbsp;: stagnation séculaire, nostalgie pour un âge où, si la vie n'était pas facile pour tous, tout semblait possible, pressions de la mondialisation et des technologies et politiques néolibérales ont mis la classe moyenne à genoux et enrayé la mobilité sociale, et la crise de 2008 a achevé de pulvériser les espérances fragiles des plus démunis.

Le plan du gouvernement a été loué pour plusieurs de ses initiatives et Louis Gallois, président de la Fédération des acteurs de la solidarité, n'a pas hésité à parler de « [sursaut bienvenu](https://www.lemonde.fr/societe/article/2018/09/13/louis-gallois-le-plan-pauvrete-un-sursaut-bienvenu_5354740_3224.html) », tout en cautionnant qu'il s'agissait d'une première étape. L'ancien député Laurent Grandguillaume, qui défendait l'extension de son innovation sociale, la loi « territoires zéro chômeur longue durée », voit son [« utopie réaliste »](https://twitter.com/LGRANDGUILLAUME/status/1040488007281856512) gagner en pérennité. L'ancien haut-commissaire aux solidarités actives contre la pauvreté, Martin Hirsch, a résumé le défi auquel fait désormais face cette nouvelle stratégie : [« La rupture, ça sera de faire plier ceux qui seront sur le chemin et essayeront de maintenir des systèmes cloisonnés »](https://www.lesechos.fr/13/09/2018/lesechos.fr/0302250465358_martin-hirsch-----on-peut-franchir-une-etape-decisive---contre-la-pauvrete.htm).

Le paupérisme n'est pas qu'une affaire de revenus&nbsp;: il relève surtout de l'«&nbsp;adversité cumulative&nbsp;» (William J. Wilson) — absence d'opportunités économiques, isolement social, insécurité, difficultés de logement, transports peu fréquents, traumatisme, problèmes de santé mal contenus, formation ou orientation mal assurées, éducation de moindre qualité conspirant ensemble à maintenir les plus fragiles dans des trappes à pauvreté. De nombreux publics sont confrontés à ce phénomène&nbs;: jeunes en déperdition, réfugiés, travailleurs pauvres, sans-abris, mères seules, séniors isolés, etc. Si l'Occident a pu s'appuyer sur des décennies de protection sociale jusqu'ici pour en adoucir le choc, celle-ci est désormais mise à rude épreuve, les laissés pour compte du monde moderne sont de plus en plus nombreux, et les années 2020 s'annoncent sombres. Les Français ne s'y trompent pas, qui [estiment pour 80% d'entre eux que les enfants d'aujourd'hui seront plus mal lotis financièrement que leurs parents quand ils grandiront](http://www.pewglobal.org/wp-content/uploads/sites/2/2018/09/Pew-Research-Center_Global-Economy-Report_2018-09-18.pdf).

Nous sommes pourtant, il faut l'espérer, à l'heure d'une aube nouvelle. Comme c'était le cas pour la lutte contre le changement climatique — l'autre grand défi de l'époque — il y a à peine 15 ans, peu de choses sont faites actuellement en Occident pour anticiper cette urgence de demain qu'est l'essor de la pauvreté et de l'inégalité&nbsp;: peu d'acteurs se vouent corps et âme à la bataille pour la solidarité, les moyens, les experts et les innovations sont rares, les consciences encore guère mobilisées, les formations universitaires ou les grands sommets sur le sujet inexistants. Mais tout cela peut changer dans la décennie, comme cela a commencé à changer pour la question climatique. Et comme pour celle-ci, il est impératif de se doter de moyens pour mener ce combat.

À cet égard ^[Quel égard?], le passé est riche d'enseignements : avant l'avènement de l'ère néolibérale, au cœur de l'âge d'or du progressisme, le président Lyndon Johnson n'a rien moins que déclaré la « guerre contre la pauvreté », les luttes civiques menées par Martin Luther King l'aidant à en faire une priorité de son administration. Et au cœur du dispositif figurait un Office of Economic Opportunity (agence aux opportunités économiques) doté d'un budget conséquent dès sa création en 1964, travaillant avec la société civile, et qui attirait les fonctionnaires parmi les meilleurs, désireux qu'ils étaient d'être à l'avant-garde d'une grande mission présidentielle.

Son rôle : « éliminer le paradoxe de la pauvreté au milieu de l'abondance dans la nation en offrant à chacun des opportunités pour l'éducation et la formation, pour travailler, et pour vivre avec décence et dignité », en administrant des programmes correspondant (santé, emploi, éducation...).

Malgré une chute prodigieuse du taux de pauvreté grâce aux initiatives fédérales (de 19% en 1964 à 11% en 1973), les conservateurs, critiques de l'« assistanat » et persuadés que la « culture » des démunis faisait de ceux-ci un bloc monolithique impossible à désagréger, se sont montrés cruels à l'égard de cette agence, et finiront par la démembrer au fil du temps, substituant à la guerre contre la pauvreté une expansion insensée du système carcéral [@hinton2016war].

Cette agence correspondait aux besoins et défis d'une époque qui n'est assurément plus la nôtre. Mais l'on peut en garder le principe d'une réaction volontariste lorsqu'apparaît une situation de pauvreté à laquelle les administrations n'offrent pas un remède à la hauteur. Aujourd'hui, la France, l'un des rares grands pays occidentaux encore préservés d'un attentat populiste — notamment grâce à cet amortisseur que constitue son généreux système social — peut se montrer pionnière en anticipant une vague qui ne cesse de grandir.

Une entité dédiée pourrait s'attaquer à ces défis de manière transversale et coordonner l'action publique, sur le modèle de l'Office of Economic Opportunity. Que l'on se rassure : cet effort s'inscrirait dans une tradition bien française, la lutte contre l'indigence étant devenue une priorité au lendemain de la Révolution, notamment avec la création du remarquable Comité de mendicité.

Cette Agence aux opportunités économiques saurait répondre à deux questions clés&nbsp;: d'abord, que faire contre la pauvreté ? À cette fin, elle serait armée d'un département de recherches doté d'experts nationaux ou internationaux pour innover, apprendre, expérimenter, établir des standards, évaluer, faire usage de la _big data_, etc. Ensuite, comment le faire&nbsp;? Planification, financements et mise en œuvre des actions lui reviendraient, et elles pourraient attirer de brillants hauts fonctionnaires comme de grands acteurs de la société civile engagés dans la lutte contre la pauvreté.

Elle serait aussi neutre que faire se peut, et se détacherait du temps politique : un directeur général respecté, nommé pour des mandats de 5 ans renouvelables une fois, lui permettrait d'assurer sa mission contre vents et marées politiques, dans la durée (un point crucial) et de manière globale. Récupérant certaines missions des Ministères du Logement, de la Santé, de l'Éducation, de la Jeunesse, des Transports, de la Ville et de l'Emploi et les budgets afférents, dotée de crédits par la Caisse des dépôts et — ne serait-ce pas justice&nbsp;? — de deniers issus de la lutte contre les paradis fiscaux, elle deviendrait un interlocuteur privilégié des démunis mais aussi des associations et des acteurs de l'économie sociale et solidaire, qui pourraient harmoniser grâce à elle leur remarquable travail mais aussi s'accréditer auprès d'elle pour recevoir et gérer des fonds dans leurs domaines de prédilection.

Elle serait aussi neutre que faire se peut, et se détacherait du temps politique : un directeur général respecté, nommé pour des mandats de 5 ans renouvelables une fois, lui permettrait d'assurer sa mission contre vents et marées politiques, dans la durée (un point crucial) et de manière globale. Récupérant certaines missions des Ministères du Logement, de la Santé, de l'Éducation, de la Jeunesse, des Transports, de la Ville et de l'Emploi et les budgets afférents, dotée de crédits par la Caisse des dépôts et — ne serait-ce pas justice&nbsp;? — de deniers issus de la lutte contre les paradis fiscaux, elle deviendrait un interlocuteur privilégié des démunis mais aussi des associations et des acteurs de l'économie sociale et solidaire, qui pourraient harmoniser grâce à elle leur remarquable travail mais aussi s'accréditer auprès d'elle pour recevoir et gérer des fonds dans leurs domaines de prédilection.

Elle œuvrerait, enfin, peut-être surtout, de concert avec les régions pour développer les opportunités dans les territoires menacés d'appauvrissement structurel en les identifiant, en travaillant sur les solutions avancées dans d'autres pays européens, et en se donnant pour objectif de construire des modèles alternatifs de développement adaptés à ces territoires.

Restructurer les administrations concernées, surmonter les retranchements institutionnels ou les querelles de chapelle et autres joies de la vie politique, administrative et territoriale serait complexe. Mais y a-t-il vraiment d'autres choix ? Anticipant les mutations de la question sociale que nous connaissons, le sociologue Robert Castel notait&nbsp;:

>Ce que l'incertitude des temps paraît exiger, ce n'est pas moins d'État — sauf à s'abandonner complètement aux «&nbsp;lois&nbsp;» du marché. Ce n'est pas non plus sans doute davantage d'État — sauf à vouloir reconstruire de force l'édifice du début des années 1970, définitivement miné par la décomposition des anciens collectifs et par la montée de l'individualisme de masse. Le recours, c'est un État stratège qui redéploierait ses interventions pour accompagner ce processus d'individualisation, désamorcer ses points de tension, éviter ses cassures et rapatrier ceux qui ont basculé en deçà de la ligne de flottaison. Un État protecteur quand même car, dans une société hyperdiversifiée et rongée par l'individualisme négatif, il n'y pas de cohésion sociale sans protection sociale. [-@castel2014metamorphoses, 475]

## Bibliographie








